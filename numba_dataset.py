import cupy as cp
import numpy as np
import math
import time
import numba as nm
from numba import cuda
import torch
from torch.utils.dlpack import from_dlpack
cp.cuda.set_allocator(None)
@cuda.jit
def batch_barrier_option(d_s, T, K, B, S0, sigma, mu, r, d_normals, N_STEPS, N_PATHS, N_BATCH):
    # ii - overall thread index
    ii = cuda.threadIdx.x + cuda.blockIdx.x * cuda.blockDim.x
    stride = cuda.gridDim.x * cuda.blockDim.x
    tmp3 = math.sqrt(T/N_STEPS)
    for i in range(ii, N_PATHS * N_BATCH, stride):
        batch_id = i // N_PATHS
        path_id = i % N_PATHS
        tmp1 = mu[batch_id]*T/N_STEPS
        tmp2 = math.exp(-r[batch_id]*T)
        running_average = 0.0
        s_curr = S0[batch_id]
        for n in range(N_STEPS):

            s_curr += tmp1 * s_curr + sigma[batch_id]*s_curr*tmp3*d_normals[path_id + batch_id * N_PATHS + n * N_PATHS * N_BATCH]
            running_average = running_average + 1.0/(n + 1.0) * (s_curr - running_average)
            if i==0 and batch_id == 2:
                print(s_curr)
            if running_average <= B[batch_id]:
                break
        payoff = running_average - K[batch_id] if running_average > K[batch_id] else 0
        d_s[i] = tmp2 * payoff

class NumbaOptionDataSet(object):
    
    def __init__(self, max_len=10, number_path = 1000, batch=2, threads=512, seed=15):
        self.num = 0
        self.max_length = max_len
        self.N_PATHS = number_path
        self.N_STEPS = 365
        self.N_BATCH = batch
        self.T = np.float32(1.0)
        self.output = cp.zeros(self.N_BATCH*self.N_PATHS, dtype=cp.float32) 
        self.number_of_blocks = (self.N_PATHS * self.N_BATCH - 1) // threads + 1
        self.number_of_threads = threads
        cp.random.seed(seed)
        
    def __len__(self):
        return self.max_length
        
    def __iter__(self):
        self.num = 0
        return self
    
    def __next__(self):
        if self.num > self.max_length:
            raise StopIteration
        X = cp.random.rand(self.N_BATCH, 6, dtype=cp.float32)
        # scale the [0, 1) random numbers to the correct range for each of the option parameters
        X = X * cp.array([200.0, 0.99, 200.0, 0.4, 0.2, 0.2], dtype=cp.float32)
        # make sure the Barrier is smaller than the Strike price
        X[:, 1] = X[:, 0] * X[:, 1]
        randoms = cp.random.normal(0, 1, self.N_BATCH * self.N_PATHS * self.N_STEPS, dtype=cp.float32)
        batch_barrier_option[(self.number_of_blocks,), (self.number_of_threads,)](self.output, self.T, X[:, 0], 
                              X[:, 1], X[:, 2], X[:, 3], X[:, 4], X[:, 5], randoms, self.N_STEPS, self.N_PATHS, self.N_BATCH)
        o = self.output.reshape(self.N_BATCH, self.N_PATHS)
        Y = o.mean(axis = 1) 
        self.num += 1
        return (from_dlpack(X.toDlpack()), from_dlpack(Y.toDlpack()))
# ds = NumbaOptionDataSet(10, number_path=100000, batch=16, seed=15)
# for i in ds:
#     print(i[1])
